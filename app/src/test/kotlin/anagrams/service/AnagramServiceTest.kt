package anagrams.service

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

internal class AnagramServiceTest {
    @Test
    fun `should be true if string are anagrams`() {
        val anagramService = AnagramService()

        assertTrue(anagramService.isAnagram("binary", "brainy"))
        assertTrue(anagramService.isAnagram("anagram", "nag a ram"))
    }

    @Test
    fun `should be false if string are not anagrams`() {
        val anagramService = AnagramService()

        assertFalse(anagramService.isAnagram("hello", "heelo"))
    }
}