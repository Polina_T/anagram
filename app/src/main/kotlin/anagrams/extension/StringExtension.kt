package anagrams

fun String.toSortedArray(): IntArray? = this.lowercase().chars().sorted().toArray()

fun String.sanitize(): String = Regex("[^A-Za-z0-9]").replace(this, "")