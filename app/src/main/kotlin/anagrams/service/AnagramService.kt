package anagrams.service

import anagrams.sanitize
import anagrams.toSortedArray
import java.util.*

class AnagramService {
    fun isAnagram(s1: String, s2: String): Boolean {
        return Arrays.equals(s1.sanitize().toSortedArray(), s2.sanitize().toSortedArray())
    }
}